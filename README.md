﻿# MVP pattern example


The first tab present the following information: 
• Name 
• The current Date and Time (updated dynamically every second) 
• An Empty label

The second tab contain two segments in a segmented control: 
• The first segment contains a table that show the information received from this RSS feed http://feeds.reuters.com/reuters/businessNews 
• The second segment contains a table which is a unification of the data received from http://feeds.reuters.com/reuters/entertainment and http://feeds.reuters.com/reuters/environment. 
