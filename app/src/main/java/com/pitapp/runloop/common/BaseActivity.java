package com.pitapp.runloop.common;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.pitapp.runloop.utils.Logger;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    public final String TAG = getClass().getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d(TAG,"onResume");
    }

}
