package com.pitapp.runloop.data.feed;

import me.toptas.rssconverter.RssItem;
import rx.Observable;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public interface FeedRepository {
    Observable<RssItem> getFirstRss();

    Observable<RssItem> getSecondRss();
}
