package com.pitapp.runloop.data.feed;

import com.pitapp.runloop.models.lists.FeedList;

import me.toptas.rssconverter.RssFeed;
import me.toptas.rssconverter.RssItem;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class FeedRepositoryImpl implements FeedRepository {

    private RssService rssService;
    private FeedList results;
    private final String URL = "http://feeds.reuters.com/reuters/businessNews";
    private final String URL2 = "http://feeds.reuters.com/reuters/entertainment";
    private final String URL3 = "http://feeds.reuters.com/reuters/environment";

    public FeedRepositoryImpl(RssService rssService) {
        this.rssService = rssService;
        this.results = new FeedList();
    }

    @Override
    public Observable<RssItem> getFirstRss() {
        Observable<RssFeed> firstRssObservable = rssService.getRss(URL);

        return firstRssObservable.concatMap(new Func1<RssFeed, Observable<? extends RssItem>>() {
            @Override
            public Observable<? extends RssItem> call(RssFeed rssFeed) {
                return Observable.from(rssFeed.getItems());
            }
        }).doOnNext(new Action1<RssItem>() {
            @Override
            public void call(RssItem rssItem) {
                results.add(rssItem);
            }
        });
    }

    @Override
    public Observable<RssItem> getSecondRss() {
        Observable<RssFeed> secondRssObservable = rssService.getRss(URL2);
        Observable<RssFeed> thirdRssObservable = rssService.getRss(URL3);
        Observable<RssFeed> result = Observable.concat(secondRssObservable, thirdRssObservable);

        return result.concatMap(new Func1<RssFeed, Observable<? extends RssItem>>() {
            @Override
            public Observable<? extends RssItem> call(RssFeed rssFeed) {
                return Observable.from(rssFeed.getItems());
            }
        }).doOnNext(new Action1<RssItem>() {
            @Override
            public void call(RssItem rssItem) {
                results.add(rssItem);
            }
        });
    }
}


