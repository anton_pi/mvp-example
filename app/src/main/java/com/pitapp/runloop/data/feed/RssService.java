package com.pitapp.runloop.data.feed;

import me.toptas.rssconverter.RssFeed;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Anton Pitaev on 17/11/17.
 */

public interface RssService {
    @GET
    Observable<RssFeed> getRss(@Url String url);
}
