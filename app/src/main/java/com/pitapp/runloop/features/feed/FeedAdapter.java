package com.pitapp.runloop.features.feed;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pitapp.runloop.R;
import com.pitapp.runloop.models.lists.FeedList;
import com.pitapp.runloop.root.App;

import me.toptas.rssconverter.RssItem;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedItemViewHolder> {

    private FeedList list;
    private FeedMVP.Presenter presenter;

    FeedAdapter(FeedList list, FeedMVP.Presenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public FeedItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        return new FeedItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeedItemViewHolder holder, int position) {
        final RssItem item = list.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvDate.setText(item.getPublishDate());

        holder.rlItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showWebDialog(item);
                App.getInstance().getSettings().setSelectedItem(item.getTitle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class FeedItemViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlItem;
        TextView tvTitle;
        TextView tvDate;

        FeedItemViewHolder(View itemView) {
            super(itemView);
            rlItem = itemView.findViewById(R.id.rl_item);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDate = itemView.findViewById(R.id.tv_date);

        }
    }
}
