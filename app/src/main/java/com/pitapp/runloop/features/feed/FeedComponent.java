package com.pitapp.runloop.features.feed;

import com.pitapp.runloop.data.feed.RssModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Singleton
@Component(modules = {FeedModule.class,RssModule.class})
public interface FeedComponent {
    void inject(FeedFragment target);
}

