package com.pitapp.runloop.features.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pitapp.runloop.R;
import com.pitapp.runloop.common.BaseFragment;
import com.pitapp.runloop.models.lists.FeedList;
import com.pitapp.runloop.root.App;
import com.pitapp.runloop.ui.dialogs.DescriptionDialog;
import com.pitapp.runloop.ui.main_activity.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.toptas.rssconverter.RssItem;

public class FeedFragment extends BaseFragment implements FeedMVP.View {

    @BindView(R.id.recycler_first)
    RecyclerView recyclerFirst;

    @BindView(R.id.recycler_second)
    RecyclerView recyclerSecond;

    @Inject
    FeedMVP.Presenter presenter;
    private boolean updateFeedThreadRunning = false;

    private FeedAdapter firstFeedAdapter, secondFeedAdapter;
    private FeedList firstFeedData = new FeedList(), secondFeedData = new FeedList();
    private boolean firstLoaded, secondLoaded;

    private Unbinder unbinder;

    public FeedFragment() {

    }

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerFeedComponent
                .builder()
                .feedModule(new FeedModule())
                .build()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        setupViews();
        setupUpdateThread();

    }

    protected void setupViews() {
        firstFeedAdapter = new FeedAdapter(firstFeedData, presenter);

        recyclerFirst.setAdapter(firstFeedAdapter);
        recyclerFirst.setItemAnimator(new DefaultItemAnimator());
        recyclerFirst.setHasFixedSize(true);
        recyclerFirst.setLayoutManager(new LinearLayoutManager(getActivity()));

        secondFeedAdapter = new FeedAdapter(secondFeedData, presenter);

        recyclerSecond.setAdapter(secondFeedAdapter);
        recyclerSecond.setItemAnimator(new DefaultItemAnimator());
        recyclerSecond.setHasFixedSize(true);
        recyclerSecond.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter.setView(this);
        presenter.getFirstFeed();
        presenter.getSecondFeed();
    }

    private void setupUpdateThread() {
        final Thread updateFeedThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(5000);
                        if (updateFeedThreadRunning) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (App.getInstance().isNetworkAvailable()) {
                                        presenter.getFirstFeed();
                                        presenter.getSecondFeed();
                                    }
                                }
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        updateFeedThread.start();
        updateFeedThreadRunning = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        updateFeedThreadRunning = false;
        presenter.unsubscribeRx();
        unbinder.unbind();
        presenter.setView(null);
    }

    @Override
    public void updateFirstFeed(RssItem item) {
        if (!firstFeedData.containsByLink(item.getLink())) {
            firstFeedData.add(item);
            firstFeedAdapter.notifyItemInserted(firstFeedData.size() - 1);
        }
    }

    @Override
    public void updateSecondFeed(RssItem item) {
        if (!secondFeedData.containsByLink(item.getLink())) {
            secondFeedData.add(item);
            secondFeedAdapter.notifyItemInserted(secondFeedData.size() - 1);
        }
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setFirstLoaded(boolean firstLoaded) {
        this.firstLoaded = firstLoaded;
        showLoading(!firstLoaded);
    }

    @Override
    public void setSecondLoaded(boolean secondLoaded) {
        this.secondLoaded = secondLoaded;
        showLoading(!secondLoaded);
    }

    @Override
    public void showDialog(RssItem item) {
        new DescriptionDialog(getActivity(), item.getTitle(), item.getDescription());
    }

    private void showLoading(boolean show) {
        if (show) {
            ((MainActivity) getActivity()).showLoading(true);
        } else {
            if (firstLoaded && secondLoaded) {
                ((MainActivity) getActivity()).showLoading(false);
            }
        }
    }

}
