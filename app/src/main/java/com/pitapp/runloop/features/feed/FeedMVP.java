package com.pitapp.runloop.features.feed;

import com.pitapp.runloop.common.BasePresenter;
import com.pitapp.runloop.common.BaseView;

import me.toptas.rssconverter.RssItem;
import rx.Observable;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public interface FeedMVP {
    interface View extends BaseView {
        void updateFirstFeed(RssItem item);

        void updateSecondFeed(RssItem item);

        void showMessage(String msg);

        void showDialog(RssItem item);

        void setFirstLoaded(boolean loaded);

        void setSecondLoaded(boolean loaded);
    }

    interface Presenter extends BasePresenter {
        void setView(FeedMVP.View view);

        void getFirstFeed();

        void getSecondFeed();

        void unsubscribeRx();

        void showWebDialog(RssItem item);
    }

    interface Model {
        Observable<RssItem> firstResult();

        Observable<RssItem> secondResult();
    }
}
