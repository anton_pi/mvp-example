package com.pitapp.runloop.features.feed;

import com.pitapp.runloop.data.feed.FeedRepository;

import me.toptas.rssconverter.RssItem;
import rx.Observable;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class FeedModel implements FeedMVP.Model {

    private FeedRepository repository;

    FeedModel(FeedRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<RssItem> firstResult() {
        return repository.getFirstRss();
    }

    @Override
    public Observable<RssItem> secondResult() {
        return repository.getSecondRss();
    }
}
