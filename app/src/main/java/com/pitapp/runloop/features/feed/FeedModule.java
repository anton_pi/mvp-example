package com.pitapp.runloop.features.feed;

import com.pitapp.runloop.data.feed.FeedRepository;
import com.pitapp.runloop.data.feed.FeedRepositoryImpl;
import com.pitapp.runloop.data.feed.RssService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Module
public class FeedModule {
    @Provides
    FeedMVP.Presenter provideFeedPresenter(FeedMVP.Model model) {
        return new FeedPresenter(model);
    }

    @Provides
    FeedMVP.Model provideFeedModel(FeedRepository repository) {
        return new FeedModel(repository);
    }

    @Singleton
    @Provides
    FeedRepository provideFeedRepository(RssService rssService) {
        return new FeedRepositoryImpl(rssService);
    }
}
