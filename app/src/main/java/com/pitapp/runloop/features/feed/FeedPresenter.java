package com.pitapp.runloop.features.feed;

import android.support.annotation.Nullable;

import com.pitapp.runloop.R;
import com.pitapp.runloop.root.App;

import me.toptas.rssconverter.RssItem;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class FeedPresenter implements FeedMVP.Presenter {
    @Nullable
    private FeedMVP.View view;
    private FeedMVP.Model model;
    private Subscription firstSubscription = null, secondSubscription = null;

    FeedPresenter(FeedMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(@Nullable FeedMVP.View view) {
        this.view = view;
    }

    @Override
    public void getFirstFeed() {
        if (view != null) {
            view.setFirstLoaded(false);
        }
        firstSubscription = model.firstResult().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<RssItem>() {
            @Override
            public void onCompleted() {
                if (view != null) {
                    view.setFirstLoaded(true);
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (view != null) {
                    view.showMessage(App.getInstance().getString(R.string.first_rss_error));
                    view.setFirstLoaded(true);
                }
            }

            @Override
            public void onNext(RssItem rssItem) {
                if (view != null) {
                    view.updateFirstFeed(rssItem);
                }
            }
        });
    }

    @Override
    public void getSecondFeed() {
        if (view != null) {
            view.setSecondLoaded(false);
        }
        secondSubscription = model.secondResult().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<RssItem>() {
            @Override
            public void onCompleted() {
                if (view != null) {
                    view.setSecondLoaded(true);
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (view != null) {
                    view.showMessage(App.getInstance().getString(R.string.second_rss_error));
                    view.setSecondLoaded(true);
                }
            }

            @Override
            public void onNext(RssItem rssItem) {
                if (view != null) {
                    view.updateSecondFeed(rssItem);
                }
            }
        });
    }

    @Override
    public void unsubscribeRx() {
        if (firstSubscription != null) {
            if (!firstSubscription.isUnsubscribed()) {
                firstSubscription.unsubscribe();
            }
        }
        if (secondSubscription != null) {
            if (!secondSubscription.isUnsubscribed()) {
                secondSubscription.unsubscribe();
            }
        }
    }

    @Override
    public void showWebDialog(RssItem item) {
        if (view != null) {
            view.showDialog(item);
        }
    }

}
