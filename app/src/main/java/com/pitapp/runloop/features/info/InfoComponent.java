package com.pitapp.runloop.features.info;


import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Singleton
@Component(modules = {InfoModule.class})
public interface InfoComponent {
    void inject(InfoFragment target);
}
