package com.pitapp.runloop.features.info;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pitapp.runloop.R;
import com.pitapp.runloop.common.BaseFragment;
import com.pitapp.runloop.root.App;

import java.text.SimpleDateFormat;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class InfoFragment extends BaseFragment implements InfoMVP.View {

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_label)
    TextView tvLabel;

    private Unbinder unbinder;

    @Inject
    InfoMVP.Presenter presenter;
    private boolean updateDateThreadRunning = false;


    public InfoFragment() {

    }

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInfoComponent
                .builder()
                .infoModule(new InfoModule())
                .build()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        presenter.setView(this);
        presenter.startDateTask();
        tvLabel.setText(App.getInstance().getSettings().getSelectedItem());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        updateDateThreadRunning = false;
        unbinder.unbind();
        presenter.setView(null);
    }

    @Override
    public void updateDate() {
        final Thread updateDateThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        if (updateDateThreadRunning) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    long date = System.currentTimeMillis();
                                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy\nhh:mm:ss a");
                                    String dateString = sdf.format(date);
                                    if (tvDate != null) {
                                        tvDate.setText(dateString);
                                    }
                                }
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        updateDateThread.start();
        updateDateThreadRunning = true;
    }

    @Override
    public void updateLabel() {
        tvLabel.setText(App.getInstance().getSettings().getSelectedItem());
    }
}
