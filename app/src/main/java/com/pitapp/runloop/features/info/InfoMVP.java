package com.pitapp.runloop.features.info;

import com.pitapp.runloop.common.BasePresenter;
import com.pitapp.runloop.common.BaseView;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public interface InfoMVP {
    interface View extends BaseView {
        void updateDate();
        void updateLabel();
    }

    interface Presenter extends BasePresenter {
        void setView(InfoMVP.View view);
        void startDateTask();
    }

    interface Model {

    }
}
