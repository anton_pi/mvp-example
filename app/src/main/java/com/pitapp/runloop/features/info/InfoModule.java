package com.pitapp.runloop.features.info;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Module
class InfoModule {
    @Provides
    InfoMVP.Presenter provideInfoPresenter(){
        return new InfoPresenter();
    }

}
