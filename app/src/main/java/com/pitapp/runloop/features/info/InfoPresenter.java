package com.pitapp.runloop.features.info;

import android.support.annotation.Nullable;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class InfoPresenter implements InfoMVP.Presenter {
    @Nullable
    private InfoMVP.View view;
    private InfoMVP.Model model;

    InfoPresenter() {

    }

    public InfoPresenter(InfoMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(InfoMVP.View view) {
        this.view = view;
    }

    @Override
    public void startDateTask() {
        if(view!=null){
            view.updateDate();
        }
    }
}
