package com.pitapp.runloop.models.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import me.toptas.rssconverter.RssItem;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class FeedList extends ArrayList<RssItem> {

    public boolean containsByLink(String link) {
        for (RssItem item : this) {
            if (item.getLink().equals(link)) {
                return true;
            }
        }
        return false;
    }
   

}
