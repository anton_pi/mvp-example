package com.pitapp.runloop.root;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class App extends Application {

    private static App singleton;
    private ApplicationComponent component;
    @Inject
    AppSettings appSettings;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        singleton = this;
        appSettings = getSettings();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        component.inject(this);
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public static App getInstance() {
        return singleton;
    }

    public AppSettings getSettings() {
        // if (appSettings != null) {
        return appSettings;
//        } else {
//            return new AppSettings(this);
//        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
