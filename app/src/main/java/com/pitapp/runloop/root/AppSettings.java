package com.pitapp.runloop.root;

import android.content.Context;

import com.pitapp.runloop.utils.SettingsUtils;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class AppSettings {
    private Context context;

    AppSettings(Context context) {
        this.context = context;
    }

    public void setSelectedItem(String title){
        SettingsUtils.saveStringToSettings(context,SettingsConstants.SELECTED_ITEM,title);
    }

    public String getSelectedItem(){
        return SettingsUtils.readStringFromSettings(context,SettingsConstants.SELECTED_ITEM,"");
    }


    private class SettingsConstants{
        static final String SELECTED_ITEM = "si";
    }
}
