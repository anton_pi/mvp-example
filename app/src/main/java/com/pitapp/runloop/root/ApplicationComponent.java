package com.pitapp.runloop.root;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(App target);
}
