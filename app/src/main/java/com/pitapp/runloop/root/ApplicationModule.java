package com.pitapp.runloop.root;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Module
class ApplicationModule {

    private Application application;

    ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    AppSettings provideSettings() {
        return new AppSettings(provideContext());
    }

}
