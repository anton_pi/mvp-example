package com.pitapp.runloop.ui.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pitapp.runloop.R;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class DescriptionDialog {
    private Context context;
    private String title, description;

    public DescriptionDialog(@NonNull Context context, String title, String description) {
        this.context = context;
        this.title = title;
        this.description = description;
        createDialog();
    }


    private void createDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(this.title);

        WebView wv = new WebView(context);
        wv.loadData(this.description, "text/html", null);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        alert.setView(wv);
        alert.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();

    }
}
