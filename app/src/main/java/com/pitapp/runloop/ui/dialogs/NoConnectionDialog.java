package com.pitapp.runloop.ui.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.pitapp.runloop.R;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class NoConnectionDialog {
    private Context context;

    public NoConnectionDialog(@NonNull Context context) {
        this.context = context;
        createDialog();
    }


    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle(R.string.no_internet_title);
        builder.setMessage(R.string.no_internet_message);
        builder.setCancelable(true);

        builder.setIcon(R.drawable.ic_sync_problem_accent_24dp);

        builder.setNegativeButton(
                R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }
}


