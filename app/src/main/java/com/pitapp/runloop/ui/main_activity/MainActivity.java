package com.pitapp.runloop.ui.main_activity;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.pitapp.runloop.R;
import com.pitapp.runloop.common.BaseActivity;
import com.pitapp.runloop.features.info.InfoFragment;
import com.pitapp.runloop.ui.dialogs.NoConnectionDialog;
import com.pitapp.runloop.utils.NetworkStateReceiver;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity implements MainActivityMVP.View,NetworkStateReceiver.NetworkStateListener {
    private NetworkStateReceiver stateReceiver;

    @Inject
    @Nullable
    MainSectionsPagerAdapter sectionsPagerAdapter;

    @Inject
    MainActivityMVP.Presenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.pb)
    ProgressBar progressBar;

    @BindView(R.id.iv_connection)
    ImageView ivConnection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerMainActivityComponent
                .builder()
                .mainActivityModule(new MainActivityModule(this))
                .build()
                .inject(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        stateReceiver = new NetworkStateReceiver(this);
        initViews();
    }

    protected void initViews() {
        setSupportActionBar(toolbar);

        viewPager.setAdapter(sectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        if (sectionsPagerAdapter != null) {
                            ((InfoFragment) sectionsPagerAdapter.getItem(position)).updateLabel();
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ivConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NoConnectionDialog(MainActivity.this);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(NetworkStateReceiver.NETWORK_RECEIVER_ACTION);
        registerReceiver(stateReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(stateReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onNetworkStateChange(boolean hasConnection) {
        ivConnection.setVisibility(hasConnection ? View.GONE : View.VISIBLE);
    }
}
