package com.pitapp.runloop.ui.main_activity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Singleton
@Component(modules = {MainActivityModule.class})
public interface MainActivityComponent {
    void inject(MainActivity target);
}