package com.pitapp.runloop.ui.main_activity;

import com.pitapp.runloop.common.BasePresenter;
import com.pitapp.runloop.common.BaseView;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public interface MainActivityMVP {
    interface View extends BaseView {
        void showLoading(boolean show);
    }

    interface Presenter extends BasePresenter {
        void setView(MainActivityMVP.View view);
    }
}
