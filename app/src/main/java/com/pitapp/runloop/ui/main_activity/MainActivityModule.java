package com.pitapp.runloop.ui.main_activity;


import dagger.Module;
import dagger.Provides;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

@Module
class MainActivityModule {

    private final MainActivity activity;

    MainActivityModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    MainActivityMVP.Presenter provideFeedActivityPresenter() {
        return new MainActivityPresenter();
    }

    @Provides
    MainSectionsPagerAdapter provideFeedSectionsPagerAdapter() {
        return new MainSectionsPagerAdapter(activity.getSupportFragmentManager());
    }
}
