package com.pitapp.runloop.ui.main_activity;

import android.support.annotation.Nullable;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class MainActivityPresenter implements MainActivityMVP.Presenter {

    @Nullable
    private MainActivityMVP.View view;

    MainActivityPresenter() {
    }

    @Override
    public void setView(@Nullable MainActivityMVP.View view) {
        this.view = view;
    }

}
