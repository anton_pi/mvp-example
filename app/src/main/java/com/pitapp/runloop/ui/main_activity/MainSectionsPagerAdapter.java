package com.pitapp.runloop.ui.main_activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.pitapp.runloop.R;
import com.pitapp.runloop.common.BaseFragment;
import com.pitapp.runloop.features.feed.FeedFragment;
import com.pitapp.runloop.features.info.InfoFragment;
import com.pitapp.runloop.root.App;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class MainSectionsPagerAdapter extends FragmentPagerAdapter {

    private BaseFragment[] fragments;

    MainSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new BaseFragment[2];
        fragments[0] = InfoFragment.newInstance();
        fragments[1] = FeedFragment.newInstance();
    }

    @Override
    public Fragment getItem(int position) {
        BaseFragment fragment = fragments[position];
        if (fragment == null) {
            switch (position) {
                default:
                case 0:
                    fragment = InfoFragment.newInstance();
                    fragments[0] = fragment;
                case 1:
                    fragment = FeedFragment.newInstance();
                    fragments[1] = fragment;
            }
        }
        return fragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object ret = super.instantiateItem(container, position);
        switch (position) {
            case 0:
                fragments[position] = (InfoFragment) ret;
                return ret;
            case 1:
                fragments[position] = (FeedFragment) ret;
                return ret;
        }
        return ret;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return App.getInstance().getResources().getString(R.string.title_info);
            case 1:
                return App.getInstance().getResources().getString(R.string.title_feed);
        }
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


}
