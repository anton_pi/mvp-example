package com.pitapp.runloop.utils;

import android.util.Log;

import com.pitapp.runloop.BuildConfig;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class Logger {
    private final static boolean DEBUG = BuildConfig.DEBUG;

    public static void d(String tag, String message) {
        if (DEBUG) {
            if (tag == null) {
                tag = "Runloop";
            }
            Log.d(tag, message + "");
        }
    }
}
