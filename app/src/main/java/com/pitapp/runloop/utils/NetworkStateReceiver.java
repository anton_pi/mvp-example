package com.pitapp.runloop.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class NetworkStateReceiver extends BroadcastReceiver {

    public static final String TAG = NetworkStateReceiver.class.getSimpleName();
    public static final String NETWORK_RECEIVER_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private boolean online = true;
    private NetworkStateListener listener;

    public NetworkStateReceiver(NetworkStateListener listener) {
        this.listener = listener;
    }

    public void onReceive(Context context, Intent intent) {
        Logger.d(TAG,"Network connectivity change");
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = manager.getActiveNetworkInfo();
        if (ni == null || ni.getState() != NetworkInfo.State.CONNECTED) {
            Log.d(TAG,"There's no network connectivity");
            if (online)
            online = false;
        } else {
            Log.d(TAG,"Network "+ni.getTypeName()+" connected");
            if (!online)
            online = true;
        }
        listener.onNetworkStateChange(online);
    }

    public interface NetworkStateListener{
        void onNetworkStateChange(boolean hasConnection);
    }
}
