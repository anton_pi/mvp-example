package com.pitapp.runloop.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Anton Pitaev on 18/11/17.
 */

public class SettingsUtils {
    private static final String PREFS_NAME = "rpn";

    public static void saveIntToSettings(Context context, String key, int value) {
        try {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(key, value);
            editor.apply();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static int readIntFromSettings(Context context, String key, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getInt(key, defaultValue);
    }

    public static void saveLongToSettings(Context context, String key, long value) {
        try {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putLong(key, value);
            editor.apply();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static long readLongFromSettings(Context context, String key, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getLong(key, defaultValue);
    }

    public static void saveBooleanToSettings(Context context, String key, boolean value) {
        try {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(key, value);
            editor.apply();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static boolean readBooleanFromSettings(Context context, String key, boolean defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getBoolean(key, defaultValue);
    }

    public static void saveStringToSettings(Context context, String key, String value) {
        try {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static String readStringFromSettings(Context context, String key, String defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(key, defaultValue);
    }

}
